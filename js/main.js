$(function() {
    //search
    $('.icon__search').on('click', function() {
        $('.form-search').toggleClass('active');
    });

    //carusel
    $('.carusel-goods').slick({
        infinite: true,
        arrows: true,
        dots: true,
        adaptiveHeight: true,
        centerMode: true,
        slidesToShow: 5,
        dotsClass: 	'products-carusel-dots',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                  arrows: false,
                }
              },
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 3,
              centerPadding: '40px'
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1,
              centerPadding: '60px'
            }
          },
          {
            breakpoint: 425,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1,
              centerPadding: '100px'
            }
          }
        ]
    });

    //animation scrioll
    var $page = $('html, body');
    $('a[href*="#"]').click(function() {
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 400);
        return false;
    });
});